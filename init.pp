
file {'/etc/apt/sources.list.d/sbt.list':
  ensure  => 'present',
  content => 'deb http://dl.bintray.com/sbt/debian /'
}

package {'sbt_0.13.8':
  name   => 'sbt',
  ensure => '0.13.8'
}

package {'git_1:1.9.1-1ubuntu0.1':
  name   => 'git',
  ensure => '1:1.9.1-1ubuntu0.1'
}

package {'python_3':
  name   => 'python3',
  ensure => '3.4.0-0ubuntu2'
}

package {'pip':
  name  => 'pip',
  provider => pip3,
  ensure => '7.0.1'
}

package {'postgresql-9.3.4-1':
  name   => 'postgresql-9.3',
  ensure => '9.3.4-1'
}

package {'postgresql-9.3-postgis-2.1':
  name => 'postgresql-9.3-postgis-2.1',
  ensure => '2.1.2+dfsg-2'
}

package {'pgadmin3':
  name => 'pgadmin3',
  ensure => '1.18.1-2',
}

package {'protlib':
  name     => 'protlib',
  provider => pip3,
  ensure   => '1.4'
}

package {'crcmod':
  name     => 'crcmod',
  provider => pip3,
  ensure   => '1.7'
}


package {'kafka-python-1.2.4':
  name     => 'kafka-python',
  provider => pip3,
  ensure   => '1.2.4'
}


package {'joe':
  name     => 'joe',
  provider => pip3,
  ensure   => '0.0.6'
}

package {'avro-python3':
  name     => 'avro-python3',
  provider => pip3,
  ensure   => '1.8.0'
}

package {'geojson-1.3.2':
  name     => 'geojson',
  provider => pip3,
  ensure   => '1.3.2'
}

package {'bottle-0.12.9':
  name     => 'bottle',
  provider => pip3,
  ensure   => '0.12.9'
}

package {'mongodb':
  name => 'mongodb',
  ensure => '1:2.4.9-1ubuntu2'
}

package {'pymongo':
  name => 'pymongo',
  provider => pip3,
  ensure =>  '3.0.3'
}

package {'requests':
  name => 'requests',
  provider => pip3,
  ensure => '2.7.0'
}

package {'python3-pip':
  name => 'python3-pip',
  ensure => '1.5.4-1'
}

package {'quick_orm 0.4.4':
  name => 'quick_orm',
  ensure => '0.4.4',
  provider => pip3
}

package {'sqlalchemy':
  name => 'sqlalchemy',
  ensure => '1.0.8',
  provider => pip3
}

package {'psycopg2':
  name => 'psycopg2',
  ensure => '2.6.1',
  provider => pip3
}

package {'gdal':
  name => 'gdal',
  ensure => '2.0.0',
  provider => pip3
}

package {'libpq-dev':
  name => 'libpq-dev',
  ensure => present
}
package {'python3-dev':
  name => 'python3-dev',
  ensure => present
}

package {'python3-all-dev':
  name => 'python3-all-dev',
  ensure => present
}

package {'python3-numpy':
  name => 'python3-numpy',
  ensure => present
}
 package {'python3-scipy':
  name => 'python3-scipy',
  ensure => present
}
 package {'python3-matplotlib':
  name => 'python3-matplotlib',
  ensure => present
}
 package {'ipython3':
  name => 'ipython3',
  ensure => present
}
 package {'ipython3-notebook':
  name => 'ipython3-notebook',
  ensure => present
}
 package {'python3-pandas':
  name => 'python3-pandas',
  ensure => present
}
 package {'python3-sympy':
  name => 'python3-sympy',
  ensure => present
}
 package {'python3-nose':
  name => 'python3-nose',
  ensure => present
}

 package {'python3-pyproj':
  name => 'python3-pyproj',
  ensure => present
}
 package {'python3-shapely':
  name => 'python3-shapely',
  ensure => present
}

package {'basemap':
  name => 'basemap',
  ensure => '1.0.7',
  provider => pip3
}

package {'fiona':
  name => 'fiona',
  ensure => '1.6.1',
  provider => pip3
}

 package {'libgeos-dev':
  name => 'libgeos-dev',
  ensure => present
}

 package {'ant':
  name => 'ant',
  ensure => present
}

 package {'tomcat7':
  name => 'tomcat7',
  ensure => present
}

 package {'mysql-server-5.6':
  name => 'mysql-server-5.6',
  ensure => present
}

 package {'mysql-client-5.6':
  name => 'mysql-client-5.6',
  ensure => present
}

 package {'libmysql-java':
  name => 'libmysql-java',
  ensure => present
}

 package {'geopy':
  name => 'geopy',
  provider => pip3 ,
  ensure => '1.11.0'
}

package {'json-rpc-1.10.3':
  name     => 'json-rpc',
  provider => pip3,
  ensure   => '1.10.3'
}

package {'Werkzeug-0.11.10':
  name      => 'Werkzeug',
  ensure    => '0.11.10',
  provider  => pip3
}
