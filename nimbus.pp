package {'uuid-dev':
  name   => 'uuid-dev',
  ensure => 'present'
}

package {'libtool':
  name   => 'libtool',
  ensure => '2.4.2-1.7ubuntu1'
}

package {'autoconf':
  name   => 'autoconf',
  ensure => '2.69-6'
}

group {'storm_group':
  name => 'storm',
  ensure => 'present',
  gid => '53001'
}

file {['/app','/app/home']:
  ensure => 'directory',
  owner  => root,
  group  => root,
  mode   => 0755
}

user {'storm_user':
  name => 'storm',
  gid  => '53001',
  uid  => '53001',
  home  => '/app/home/storm',
  shell => '/bin/bash',
  comment => 'Storm service account',
  expiry => 'absent',
  password_max_age => '-1',
  password_min_age => '-1',
  require => [File['/app/home'], Group['storm_group']],
}

file {'storm_home':
  ensure => 'directory',
  name => '/app/home/storm',
  mode => 0700,
  owner => 'storm',
  group => 'storm',
  require => User['storm_user']
}

file {'/app/storm':
  ensure => 'directory',
  owner => 'storm',
  group => 'storm',
  mode  => 0750,
  require => File['/app']
}

$ip = $::ipaddress
notice("IP $ip")
host {'nimbus1':
  ip => $ip,
  host_aliases => 'nimbus'
}

host {'zkserver1':
  ip => $ip,
}
host {'slave1':
  ip => $ip,
}
